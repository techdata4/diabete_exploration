# Analyse des Données pour la Prédiction du Diabète

## Introduction

Ce document propose une analyse des jeux de données disponibles pour prédire le diabète chez les patients. 

L'objectif est de concevoir un modèle logique de données (MLD) qui stocke les informations essentielles pour évaluer si une personne est diabétique ou non.


## Les données de chaque dataframe

### 1er dataframe :

| Attribut                 | Description                             |
|--------------------------|-----------------------------------------|
| Pregnancies              | Nombre de grossesses                    |
| Glucose                  | Niveau de glucose dans le sang          |
| BloodPressure            | Mesure de la pression artérielle        |
| SkinThickness            | Épaisseur de la peau                    |
| Insulin                  | Niveau d'insuline dans le sang          |
| BMI                      | Indice de masse corporelle              |
| DiabetesPedigreeFunction | Pourcentage de diabète                  |
| Age                      | Âge                                     |
| Outcome                  | Résultat final (1 pour Oui, 0 pour Non) |

### 2ème dataset :

| Attribut | Description                                    |
|----------|------------------------------------------------|
| ID       | Identifiant                                    |
| chol     | Cholestérol total                              |
| stab.glu | Glucose sanguin stable                         |
| hdl      | Lipoprotéines de haute densité                 |
| ratio    | Ratio                                          |
| glyhb    | Taux de glycosylée dans l'hémoglobine(le sang) |
| location | Lieu                                           |
| age      | Âge                                            |
| gender   | Genre                                          |
| height   | Taille                                         |

### 3ème dataset :

| Attribut             | Description                                                                               |
|----------------------|-------------------------------------------------------------------------------------------|
| Diabetes_012         | 0 = pas de diabète, 1 = prédiabète, 2 = diabète                                           |
| HighBP               | 0 = pas d'hypertension, 1 = hypertension                                                  |
| HighChol             | 0 = pas de cholestérol élevé, 1 = cholestérol élevé                                       |
| CholCheck            | 0 = pas de vérification du cholestérol en 5 ans, 1 = vérification du cholestérol en 5 ans |
| BMI                  | Indice de masse corporelle                                                                |
| Smoker               | Avez-vous fumé au moins 100 cigarettes dans votre vie ? 0 = non, 1 = oui                  |
| Stroke               | Avez-vous déjà eu un AVC ? 0 = non, 1 = oui                                               |
| HeartDiseaseorAttack | Maladie coronarienne ou infarctus du myocarde 0 = non, 1 = oui                            |
| PhysActivity         | Activité physique au cours des 30 derniers jours - en dehors du travail 0 = non, 1 = oui  |
| FruitsConsume        | Consommez-vous des fruits 1 fois ou plus par jour ? 0 = non, 1 = oui                      |

### 4ème dataset :

note : ce dataset est composé de plusieurs fichier csv.

| Attribut            | Description                    |
|---------------------|--------------------------------|
| Age                 | Âge                            |
| Gender              | Genre                          |
| BMI                 | Indice de masse corporelle     |
| Hypertension        | Hypertension                   |
| Heart Disease       | Maladie cardiaque              |
| Smoking History     | Antécédents de tabagisme       |
| HbA1c Level         | Niveau d'HbA1c                 |
| Blood Glucose Level | Niveau de glucose dans le sang |

